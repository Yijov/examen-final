﻿using System;




namespace examen_final
{
    public class Log
    {
        public static int intentos = 0;//cuenta cuantas veces se ha intentado loguear
        public static void Login()
        {

            //LogIn

            Console.WriteLine("Por favor ingrese su código y contraseña para accesar");
            Console.Write("Usuario: ");
            string usuario= Convert.ToString (Console.ReadLine());
            Console.Write("Constraseña: ");
            int contraseña = Convert.ToInt32(Console.ReadLine());
            
               
            //validación
            if (intentos == 3)
            {
                Console.WriteLine("Usted ha excedido el numero de intentos permitidos");
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("Cerrando sistema");
                
            }else{

                if (User.userList.Exists(element => element.userName==usuario && element.password==contraseña))
                {   
                    
                    User accessUser= User.userList.Find(element => element.userName==usuario && element.password==contraseña);
                    if(accessUser.active == true){
                        
                        Console.WriteLine("Acabas de ingresar con el usuario {0}  y su rol es {1}", usuario, accessUser.rol);

                    } else{

                        Console.WriteLine("acceso Denegado");
                        Console.WriteLine("Su usuario {0} está inactivo", usuario);
                        Console.WriteLine("Contacte al administrador del sistema o ingrese con otro usuaro");
                        Console.WriteLine("");
                        Console.WriteLine("");
                        intentos++;
                        Login();
                        
                    }
                    
                } else{

                        Console.WriteLine("Usuaro o contraseña incorrectos");
                        Console.WriteLine("Por favor intente de nuevo");
                        Console.WriteLine("");
                        Console.WriteLine("");
                        intentos++;
                        Login();
                }



            }
                
        }
    }
}

