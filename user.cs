﻿using System;
using System.Collections.Generic;


namespace examen_final
{
    public class User
    {
        public string name, userName, rol;
        public int password;
        public bool active;
        public DateTime creationDate;

        public static List<User> userList = new List<User>(); //lista de usuarios para iterar
        

        public static int cantidadDeUsuarios = 0; //indica al sistema cuantos usuarios hay y se incrementa cada vez que un usuario es instanciado;
        public User (string nombre, string userName, int clave, string rol, bool actvo)
        {
            this.name = nombre;
            this.userName=userName;
            this.password=clave;
            this.rol=rol;
            this.active= actvo;
            this.creationDate = DateTime.Now;
            cantidadDeUsuarios++;//incrementa el contador de usuarios
            userList.Add(this);//Añade el usuario recien creado a la lista de usuarios
        }
    }
}

