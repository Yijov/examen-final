﻿using System;


namespace examen_final
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //Instanciando los usuarios de la clase User
            User Mario = new User("Mario Cardona", "224-0030201-3", 562016, "Administrador", true);
            User Emenegildo = new User("Emenegildo Cuevas", "224-0030201-4", 561216, "Supervisor", true);
            User Javier = new User("Javier Segundo", "224-0030201-5", 563016, "Vendedor", true);
            User Wirkinson = new User("Wirkinson Cardona", "224-0030201-6", 561816, "Vendedor", false);
            User Pablo = new User("Pablo Perez", "224-0030201-9", 561516, "Vendedor", false);
            
            Console.WriteLine("Welcome");
            Console.WriteLine("");
           
           //validación
            try
            {
                Log.Login();
                
            }
            catch (System.Exception) //previene cualquier dato mal puesto
            {
                Console.WriteLine("Usuaro o contraseña incorrectos");
                        Console.WriteLine("Por favor intente de nuevo");
                        Log.intentos++;
                        Console.WriteLine("");
                        Console.WriteLine("");
                        Log.Login();
                
            }
            
        }
    }
}

